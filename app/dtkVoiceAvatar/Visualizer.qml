// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Extras 2.15
import QtQuick 2.2 as QQ2

Entity {

    id: sceneRoot

    property int barRotationTimeMs: 1
    property int numberOfBars: 1
    property string animationState: "playing"

    Camera {
        id: camera
        projectionType: CameraLens.PerspectiveProjection
        fieldOfView: 45
        aspectRatio: 1820 / 1080
        nearPlane: 0.1
        farPlane: 1000.0
        position: Qt.vector3d(0.014, 0.956, 2.178)
        upVector: Qt.vector3d(0.0, 1.0, 0.0)
        viewCenter: Qt.vector3d(0.0, 0.7, 0.0)
    }

    Entity {
        components: [
            DirectionalLight {
                intensity: 0.9
                worldDirection: Qt.vector3d(0, 0.6, -1)
            }
        ]
    }

    RenderSettings {
        id: external_forward_renderer
        activeFrameGraph: ForwardRenderer {
            camera: camera
            clearColor: "transparent"
            showDebugOverlay: true
        }
    }

    components: [external_forward_renderer]

    CuboidMesh {
        id: barMesh
        xExtent: 0.1
        yExtent: 0.1
        zExtent: 0.1
    }

    NodeInstantiator {

        id: collection

        property int maxCount: parent.numberOfBars;

        model: maxCount

        delegate: BarEntity {

            id: cubicEntity
            entityMesh: barMesh
            rotationTimeMs: sceneRoot.barRotationTimeMs
            entityIndex: index
            entityCount: sceneRoot.numberOfBars
            entityAnimationsState: animationState
            magnitude: 0
        }
    }

    Mesh {
        id: circleMesh
        source: "qrc:/meshes/circle.obj"
    }

    Entity {
        id: circleEntity
        property Material circleMaterial: PhongAlphaMaterial {
            alpha: 0.4
            ambient: "black"
            diffuse: "black"
            specular: "black"
            shininess: 10000
        }

        components: [circleMesh, circleMaterial]
    }
}

//
// Visualizer.qml ends here
