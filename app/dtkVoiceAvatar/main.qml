// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

import QtQuick 2.0
import QtQuick.Scene3D 2.0
import QtQuick.Layouts 1.2
import QtMultimedia 5.0

Item {

    id: mainview

    width: 1215
    height: 720

    visible: true

    property int millisecondsPerBar: 68
    property int mediaLatencyOffset: 68

    Image {
        id: coverImage
        anchors.fill: parent
        source: "qrc:/images/albumcover.png"
    }

    Scene3D {
        anchors.fill: parent

        Visualizer {
            id: visualizer
            animationState: "playing"
            numberOfBars: 120
            barRotationTimeMs: 8160 // 68 ms per bar
        }
    }

    // ///////////////////////////////////////////////
    //
    // ///////////////////////////////////////////////

    Grid {

        anchors.top: parent.top;
        anchors.right: parent.right;

        height: 50 * 2 + 10 * 3;
         width: 400;

        columns: 11;
           rows: 2;

        anchors.topMargin: 30;
        anchors.leftMargin: 30;

        spacing: 10;

        Repeater {
            model: 22

            Rectangle {
                id: button;

                width: 25;
                height: 25;
                radius: 13;

                border.width: 3;
                border.color: "#666666";

                color: "#232323"

                MouseArea {
                    anchors.fill: parent;

                    hoverEnabled: true;

                    onEntered: button.color = "#464646";
                    onExited: button.color = "#232323";

                    onClicked: {
                        switch(index) {

                        case 0:
                            controller.setSource(":/mp3/ah-une-vache-pres-cest-pas-une-science-exacte.mp3");
                            subtitle.text = "A une vache pres, c'est pas une science exacte";
                            break;

                        case 1:
                            controller.setSource(":/mp3/ah_bah_alors_la_je_les_attends_les_mecs.mp3");
                            subtitle.text = "Ah bah alors la, je les attends les mecs";
                            break;

                        case 2:
                            controller.setSource(":/mp3/allez_boire_un_coup.mp3");
                            subtitle.text = "Ohhhh non, mais allez boire un coup, je vous assure, ca ira mieux !";
                            break;

                        case 3:
                            controller.setSource(":/mp3/alors-oisif-obese-autant-dire-des-gros-cons.mp3");
                            subtitle.text = "Alors oisif, obese, autant dire des gros cons";
                            break;

                        case 4:
                            controller.setSource(":/mp3/alors_ca_vient_ptite_bite.mp3");
                            subtitle.text = "Alors ca vient ??? P'TITE BITE !";
                            break;

                        case 5:
                            controller.setSource(":/mp3/bucher3.mp3");
                            subtitle.text = "Heretique au bucher !!!!";
                            break;

                        case 6:
                            controller.setSource(":/mp3/crame_ta_famille.mp3");
                            subtitle.text = "Toi un jour, je te crame ta famille toi ...";
                            break;

                        case 7:
                            controller.setSource(":/mp3/cuillere.mp3");
                            subtitle.text = "Couiiilleeeere";
                            break;

                        case 8:
                            controller.setSource(":/mp3/cuit_les_boules.mp3");
                            subtitle.text = "Ca m'a litteralement CUIT les boules";
                            break;

                        case 9:
                            controller.setSource(":/mp3/deux_trous_du_cul_soient_plus_efficaces_qu_un_seul.mp3");
                            subtitle.text = "Je ne pense pas que deux trous du cul soient plus efficaces qu'un seul ...";
                            break;

                        case 10:
                            controller.setSource(":/mp3/en_garde_espece_de_vieille_pute_degarnie.mp3");
                            subtitle.text = "En garde espece de vielle pute degarnie !!!";
                            break;

                        case 11:
                            controller.setSource(":/mp3/et-ca-cest-du-nougat.mp3");
                            subtitle.text = "Et CAAA, c'est du NOUGAT ????";
                            break;

                        case 12:
                            controller.setSource(":/mp3/fumier.mp3");
                            subtitle.text = "FUUUMIEEEEEEER";
                            break;

                        case 13:
                            controller.setSource(":/mp3/gras_du_cul.mp3");
                            subtitle.text = "Je vais vous decouper le gras du cul ca vous fera toujours ca de moins a trimbaler";
                            break;

                        case 14:
                            controller.setSource(":/mp3/he_les_connards_vous_pouvez_faire_griller_un_porcelet.mp3");
                            subtitle.text = "Ehhh les connards, vous pouvez faire griller un porcelet s'il vous plait ?";
                            break;

                        case 15:
                            controller.setSource(":/mp3/il_trouverait_meme_pas_sa_bite_pour_pisser.mp3");
                            subtitle.text = "Mais vous vous foutez de moi ??? Il a au moins 95 ans, il a une jambe raide et il est tellement bigleux qu'il ne trouverait meme pas si bite pour pisser!";
                            break;

                        case 16:
                            controller.setSource(":/mp3/interprete.mp3");
                            subtitle.text = "Interprete ? Interpreeete ! Interprete ? Interpreeeeete ! Couillleeeere";
                            break;

                        case 17:
                            controller.setSource(":/mp3/mais_allez_chier_dans_une_fiolle.mp3");
                            subtitle.text = "Mais allez chier dans une fiole on verra apres !";
                            break;

                        case 18:
                            controller.setSource(":/mp3/pas-moyen-de-piger-un-broc-de-ce-quil-dit.mp3");
                            subtitle.text = "Pas moyen de piger un broc de ce qu'il dit !";
                            break;

                        case 19:
                            controller.setSource(":/mp3/politique_de_l_autruche.mp3");
                            subtitle.text = "On dit la politique de l'autruche. Avant je ne comprenais pas. Mais maintenant que j'en ai vu une d'autruche, c'est bon ! Une politique qui court vite, une politique qui fait des gros oeufs. C'est tout !";
                            break;

                        case 20:
                            controller.setSource(":/mp3/quand_on_gueule_sans_savoir_pourquoi.mp3");
                            subtitle.text = "Quand quand on gueule sans savoir pourquoi, euhhh bon euuhh, ca ne facilite pas la negociation derriere, hein bon, alors bon.";
                            break;

                        case 21:
                            controller.setSource(":/mp3/remonte-ton-slibard.mp3");
                            subtitle.text = "Et TOC, remonte ton slibard, Lothard !";
                            break;

                        default:
                            break;
                        }

                        controller.start();
                    }
                }
            }
        }
    }

    FontLoader {
        id: font;
        source: "LemonJuice.otf";
    }

    Rectangle {

        id: subtitle_area;

        anchors.bottom: parent.bottom;
        anchors.horizontalCenter: parent.horizontalCenter;
        anchors.bottomMargin: 50;

        height: parent.height * 0.1;
        width: parent.width * 0.6;

        color: "#44000000";

        border.color: "#222222"
        border.width: 3;

        radius: 10;

        visible: controller.amplitude > 0;

        Text {
            id: subtitle;

            anchors.centerIn: parent;

            clip: true;

            elide: Text.ElideRight;

            wrapMode: Text.WordWrap;

            width: parent.width;
            height: parent.height;

            horizontalAlignment: Text.AlignHCenter;
            verticalAlignment: Text.AlignVCenter;

            color: "#ffffff";

            font.family: font.name;
            font.pointSize: 32;
        }
    }

    Rectangle {

        id: indicator;

        anchors.centerIn: parent;

        width: 200;
        height: 200;

        radius: 100;

        color: "#000000";

        border.width: 10;
        border.color: "white";

        visible: false;

        SequentialAnimation on color {

            ColorAnimation { from: "#00000000"; to: "#4400ff00"; duration: 400; }
            ColorAnimation { from: "#4400ff00"; to: "#00000000"; duration: 800; }

            loops: Animation.Infinite;

            running: true;
        }

    }

    Connections {
        target: controller

        function onDisplay(text) {
            subtitle.text = text;
        }

        function onBeginRecording() {
            indicator.visible = true;
        }

        function onEndRecording() {
            indicator.visible = false;
        }
    }
}

//
// main.qml ends here
