// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtGui>
#include <QtQuick>
#include <QtQml>
#include <QtMultimedia>
#include <QtMultimediaWidgets>
#include <QtWidgets>

#include <dtkVrMessaging>

#include "engine.h"

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

class Controller : public dtkVrMessagingClient
{
    Q_OBJECT
    Q_PROPERTY(qreal amplitude READ amplitude NOTIFY amplitudeChanged)

public:
    Controller(void);
   ~Controller(void);

signals:
    void amplitudeChanged(void);
    void display(const QString&);

signals:
    void beginRecording(void);
    void   endRecording(void);

public slots:
    void onStateChanged(void);
    void onDisconnected(void);

public slots:
    void setSource(const QString&);

public slots:
    void start(void);

public:
    qreal amplitude(void);

public:
    Engine *engine = nullptr;

public:
    qreal rms_level = 0.0;
};

Controller::Controller(void)
{
    this->engine = new Engine;

    // connect(m_engine, &Engine::stateChanged, this, &MainWidget::stateChanged);
    // connect(m_engine, &Engine::formatChanged, this, &MainWidget::formatChanged);

// /////////////////////////////////////////////////////////////////////////////
// Sound handling
// /////////////////////////////////////////////////////////////////////////////

    connect(this->engine, &Engine::stateChanged, [=] (QAudio::Mode mode, QAudio::State state) -> void
    {
        Q_UNUSED(mode);

        if (state != QAudio::StoppedState)
            return;

        this->rms_level = 0;

        emit amplitudeChanged();
    });

    connect(this->engine, &Engine::levelChanged, [=] (qreal rmsLevel, qreal peakLevel, int numSamples)
    {
        const qreal smooth = pow(qreal(0.9), static_cast<qreal>(numSamples) / 256);

        this->rms_level = (this->rms_level * smooth) + (rmsLevel * (1.0 - smooth)) * 10;

        emit amplitudeChanged();

        // qDebug() << Q_FUNC_INFO << "GOTCHA!" << rms_level; // << peakLevel << numSamples;
    });

    connect(this->engine, &Engine::infoMessage, this, [=] (const QString& message, int timeout)
    {
        // qDebug() << Q_FUNC_INFO << message << timeout;
    });

    connect(this->engine, &Engine::errorMessage, this, [=] (const QString& heading, const QString& detail)
    {
        // qDebug() << Q_FUNC_INFO << heading << detail;
    });

// /////////////////////////////////////////////////////////////////////////////
// Messaging
// /////////////////////////////////////////////////////////////////////////////

    this->setHostname("localhost");
    this->setPort(1883);

    connect(this, &dtkVrMessagingClient::stateChanged, this, &Controller::onStateChanged);
    connect(this, &dtkVrMessagingClient::disconnected, this, &Controller::onDisconnected);

    connect(this, &dtkVrMessagingClient::messageReceived, this, [this](const QByteArray &message, const dtkVrMessagingTopicName &topic) {

        const QString content = QDateTime::currentDateTime().toString()
                    + QLatin1String(" Received Topic: ")
                    + topic.name()
                    + QLatin1String(" Message: ")
                    + message
                    + QLatin1Char('\n');

        if(topic.name() == "dtk-voice/text") {

            emit display(message);
        }

        if(topic.name() == "dtk-voice/sound") {

            QFile file("/tmp/answer.mp3");
            file.open(QIODevice::WriteOnly);
            file.write(message);
            file.close();

            this->setSource("/tmp/answer.mp3");
            this->start();
        }

        if(topic.name() == "dtk-voice/control") {

            if(message == "begin")
                emit beginRecording();

            if(message == "end")
                emit endRecording();
        }
    });

    connect(this, &dtkVrMessagingClient::pingResponseReceived, this, [this]() {
        const QString content = QDateTime::currentDateTime().toString()
                    + QLatin1String(" PingResponse")
                    + QLatin1Char('\n');
        qDebug() << Q_FUNC_INFO << content;
    });

    this->connectToHost();
}

Controller::~Controller(void)
{
    delete this->engine;
}

void Controller::setSource(const QString& source)
{
    QString path = source;
    path.replace("qrc", "");

    QProcess::execute("touch", QStringList() << "/tmp/sound.mp3");
    QProcess::execute("touch", QStringList() << "/tmp/sound.wav");

    QProcess::execute("rm", QStringList() << "/tmp/sound.mp3");
    QProcess::execute("rm", QStringList() << "/tmp/sound.wav");

    QFile::copy(source, "/tmp/sound.mp3");

    QProcess::execute("ffmpeg", QStringList() << "-y" << "-i" << "/tmp/sound.mp3" << "-acodec" << "pcm_s16le" << "-ac" << "1" << "-ar" << "16000" << "/tmp/sound.wav" << "-loglevel" << "quiet"); // << "2>" << "/dev/null");

    this->engine->loadFile("/tmp/sound.wav");
}

void Controller::start(void)
{
    this->engine->startPlayback();
}

qreal Controller::amplitude(void)
{
    return this->rms_level;
}

void Controller::onStateChanged(void)
{
    const QString content = QDateTime::currentDateTime().toString()
                    + QLatin1String(": State Change ")
                    + QString::number(this->state())
                    + QLatin1Char('\n');

    if (this->state() == dtkVrMessagingClient::Connected) {

        this->subscribe(dtkVrMessagingTopicFilter("dtk-voice/text"));
        this->subscribe(dtkVrMessagingTopicFilter("dtk-voice/sound"));
        this->subscribe(dtkVrMessagingTopicFilter("dtk-voice/control"));
    }
}

void Controller::onDisconnected(void)
{

}

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication application(argc, argv);

    QLoggingCategory::setFilterRules("*.debug=false");

    QSurfaceFormat format;
    if (QOpenGLContext::openGLModuleType() == QOpenGLContext::LibGL) {
        format.setVersion(3, 2);
        format.setProfile(QSurfaceFormat::CoreProfile);
    }
    format.setDepthBufferSize(24);
    format.setStencilBufferSize(8);

    Controller *controller = new Controller;

// /////////////////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////////////////

    auto convert = [] (const QString& resource) {

        QFileInfo info(resource);

        QString path_m = QString("/tmp/%1").arg(info.fileName());
        QString path_w = QString("/tmp/%1").arg(info.fileName()).replace("mp3", "wav");

        QFile::copy(resource, path_m);

        QProcess::execute("ffmpeg", QStringList() << "-y" << "-i" << path_m << "-acodec" << "pcm_s16le" << "-ac" << "1" << "-ar" << "16000" << path_w << "-loglevel" << "quiet");
    };

    convert(":/mp3/bg1.mp3");
    convert(":/mp3/bg2.mp3");
    convert(":/mp3/bg3.mp3");

    QMediaPlaylist *playlist = new QMediaPlaylist;
    playlist->addMedia(QUrl("file:///tmp/bg1_wav"));
    playlist->addMedia(QUrl("file:///tmp/bg2.wav"));
    playlist->addMedia(QUrl("file:///tmp/bg3.wav"));
    playlist->setCurrentIndex(0);

    QMediaPlayer *player = new QMediaPlayer;
    player->setPlaylist(playlist);
    player->setVolume(20);
    player->play();

// /////////////////////////////////////////////////////////////////////////////

    QQuickView view;
    view.setFormat(format);
    view.create();
    view.rootContext()->setContextProperty("controller", controller);
    view.setSource(QUrl("qrc:/main.qml"));
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.showFullScreen();

    int status = application.exec();

    delete controller;

    return status;
}

// /////////////////////////////////////////////////////////////////////////////

#include "main.moc"

//
// main.cpp ends here
