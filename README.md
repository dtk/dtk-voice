[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](LICENSE.md)

Mycroft
==========

Mycroft is an open source voice assistant.

# Table of Contents

- [Mycroft](#mycroft)
- [Table of Contents](#table-of-contents)
- [Run Mycroft](#run-mycroft)
- [Using Mycroft](#using-mycroft)
  - [Mycroft Home Device](#mycroft-home-device)
  - [Configuration](#configuration)
  - [Using Mycroft Without Home](#using-mycroft-without-home)
      - [DeepSpeech configuration](#deepspeech-configuration)
      - [Mycroft configuration](#mycroft-configuration)
  - [TTS ENGINES](#tts-engines)


# Run Mycroft
Before runnning MyCroft, you should run `./dev_setup.sh` : It prepares your environment for running MyCroft services.

- `cd ~/MyCroft`
- `./start-mycroft.sh debug`

it will start the background services (microphone listener, skill, messagebus, and audio subsystems) as well as bringing up a text-based CLI that we can use to interact with Mycroft and see the contents of logs. 

We can stop all background services with: 
- `./stop-mycroft.sh`

# Using Mycroft

## Mycroft Home Device
\Mycroft AI, Inc. maintains a device and account management system known as Mycroft Home.

you should sign up to: https://home.mycroft.ai and pair your device using the code provided after running `./start-mycroft.sh`


## Configuration
We can modify mycroft configutration in different locations (they are listed in order of priorities) 

- `mycroft-core/mycroft/configuration/mycroft.conf`(Defaults)
- `[Mycroft Home](https://home.mycroft.ai)` (Remote)
- `/etc/mycroft/mycroft.conf`(Machine)
- `$HOME/.mycroft/mycroft.conf`(User)

Keys that exist in multiple configuration files will be overridden by the last file to contain the value. This process results in a minimal amount being written for a specific device and user, without modifying default distribution files.
## Using Mycroft Without Home
You can use mycroft vocal assistant, without pairing your device with Mycroft Home Service by modifying `$HOME/.mycroft/mycroft.conf` with the following contents:

```
{
  "skills": {
    "blacklisted_skills": [
      "mycroft-configuration.mycroftai",
      "mycroft-pairing.mycroftai"
    ]
  }
}
```
In this case, speech to text conversion will be disabled. You can set that up by using `Mozilla DeepSpeech`
To use `Mozilla DeepSpeech`, do these following steps: 

#### DeepSpeech configuration

- install deepspeech package: `pip install deepspeech` or `pip install deepspeech-gpu`
- install deepspeech server: `pip install deepspeech-server`
- start the server: `deepspeech-server --config config.json` , where the structure of config.json is the following: 
``` 
{
  "deepspeech": {
    "model" :"$HOME/Development/dtk-voice/3rd/MyCroft/Deep-Speech/deepspeech-0.9.1-models.pbmm",
    "scorer" :"$HOME/Development/dtk-voice/3rd/MyCroft/Deep-Speech/deepspeech-0.9.1-models.scorer",
    "beam_width": 500,
    "lm_alpha": 0.931289039105002,
    "lm_beta": 1.1834137581510284
  },
  "server": {
    "http": {
      "host": "0.0.0.0",
      "port": 8080,
      "request_max_size": 1048576
    }
  },
  "log": {
    "level": [
      { "logger": "deepspeech_server", "level": "DEBUG"}
    ]
  }
}
```
You can also use the wav2letter engine by running a flask server in a docker container: 
First, you need to pull the flashlight docker image : 
`docker pull flml/flashlight:cpu-consolidation-latest`
Then you need to build a container using Dockerfile: 

` cd 3rd/wav2letter/ && docker build -t wav2letter . `

One the container is built, you need to run it and execute the inference app: 
`docker run -d -p 5000:5000 --restart unless-stopped wav2letter:latest`
`docker exec -it $docker_id bash` 
`cd /root/flashlight/ && python inference.py`

#### Mycroft configuration

Add to  `$HOME/.mycroft/mycroft.config` the following section : 

```
  "stt": {
    "deepspeech_server": {
      "uri": "http://localhost:8080/stt"
    },
    "module": "deepspeech_server"
  }
  
```

## TTS ENGINES 
Mycroft has two open source TTS engines.
`Mimic`: is a fast, light-weight engine based on Carnegie Mellon University's FLITE software. Whilst the original Mimic may sound more robotic, it is able to be synthesized on your device.
 
`Mimic 2`: is an implementation of Tacotron speech synthesis. It is a fork of Keith Ito's project with additional tooling and code enhancements. Mimic 2 provides a much more natural sounding voice, however requires significant processing power to do so and is therefore cloud-based.

Use the configuration manager to edit the mycroft.conf file by running:

```
"tts": {
  "module": "mimic2",
  "mimic": {
    "voice": "ap"
  }
}

```
