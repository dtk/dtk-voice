#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 17:47:25 2020

@author: massal
"""

# %%
import torch
import argparse
import json
from asr_run import asr
from tts_run import tts
import numpy as np
import pickle
from deeppavlov import configs, build_model

def build_model_run():
    model=build_model(configs.kbqa.kbqa_cq, download=True)


    return(model)

def get_answer(question,model):

    return(model([question]))

      
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Running Deeppavlov inference.')
    parser.add_argument('--question', required=False,
                        help='question')
    parser.add_argument('--path_audio', required=False,
                        help='path_audio')
    parser.add_argument('--sec', required=False,
                        help='duration')
 
    args = parser.parse_args()

    if args.question:
        question=args.question
    else: 
        if args.path_audio:
            path_audio=args.path_audio 
            question=asr(out=path_audio)     
        else: 
            sec=int(args.sec)
            question=asr(sec=sec)
        
    print('question',question)  
    print('building model...')
    model=build_model_run()
    answer=get_answer(question,model)
    print(answer)
    tts(answer[0],'KBQA.wav')
    



