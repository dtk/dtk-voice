#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 10 10:32:50 2020

@author: massal
"""

from deeppavlov import build_model, configs

import argparse
from io import BytesIO

import sounddevice as sd
from scipy.io.wavfile import write

def asr(sec=None, out=None):
    model = build_model(configs.nemo.asr)    
    if not out:
        sr = 16000
        duration = sec
        print('Recording...')
        myrecording = sd.rec(duration*sr, samplerate=sr, channels=1)
        sd.wait()
        print('done')
        out = BytesIO()
        write(out, sr, myrecording)

    text_batch = model([out])
    
    return(text_batch[0])
sr = 16000
myrecording=sd.rec(10*sr, samplerate=sr, channels=1)

i=0
model = build_model(configs.nemo.asr) 
print('recording')
while i<1:
    myrecording= sd.rec(10*sr, samplerate=sr, channels=1)
    sd.wait()
    i+=1
out = BytesIO()
print(type(out))
print(type(myrecording))
print(myrecording)
write(out, sr, myrecording)
text_batch = model([out])
print(text_batch)






