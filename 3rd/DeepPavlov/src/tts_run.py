#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 10 10:47:04 2020

@author: massal
"""

from deeppavlov import build_model, configs
def tts(text,output_path):
    model = build_model(configs.nemo.tts)
    filepath_batch = model([text], [output_path])

    print(f'Generated speech has successfully saved at {filepath_batch[0]}')