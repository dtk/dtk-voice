## Version: $Id: $
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

project(FFT)

## #################################################################
## Sources
## #################################################################

set(${PROJECT_NAME}_HEADERS
  Array.h
  Array.hpp
  DynArray.h
  DynArray.hpp
  FFTReal.h
  FFTReal.hpp
  FFTRealFixLen.h
  FFTRealFixLen.hpp
  FFTRealFixLenParam.h
  FFTRealPassDirect.h
  FFTRealPassDirect.hpp
  FFTRealPassInverse.h
  FFTRealPassInverse.hpp
  FFTRealSelect.h
  FFTRealSelect.hpp
  FFTRealUseTrigo.h
  FFTRealUseTrigo.hpp
  OscSinCos.h
  OscSinCos.hpp
  def.h
  fftreal_wrapper.h)

set(${PROJECT_NAME}_SOURCES
  fftreal_wrapper.cpp)

## #################################################################
## Build rules
## #################################################################

add_definitions(-DSUPERIMPOSE_PROGRESS_ON_WAVEFORM)
add_definitions(-DSPECTRUM_ANALYSER_SEPARATE_THREAD)

dtk_add_library(${PROJECT_NAME} SOURCES
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_HEADERS})

dtk_link_library(${PROJECT_NAME} PUBLIC Qt5::Core)
dtk_link_library(${PROJECT_NAME} PUBLIC Qt5::Gui)

## ###################################################################
## Install rules - targets
## ###################################################################

install(TARGETS ${PROJECT_NAME} DESTINATION lib)

######################################################################
### CMakeLists.txt ends here
