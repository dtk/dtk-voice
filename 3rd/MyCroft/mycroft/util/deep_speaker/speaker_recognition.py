import numpy as np
import random
from .audio import read_mfcc
from .batcher import sample_from_mfcc
from .constants import SAMPLE_RATE, NUM_FRAMES, THRESHOLD_PREDICTION
from .conv_models import DeepSpeakerModel
from .test import batch_cosine_similarity
import time
from numpy import load
import os
def transform_voice(path_file, path_model): 
    model = DeepSpeakerModel()
    model.m.load_weights(path_model, by_name=True)
    mfcc_recorded= sample_from_mfcc(read_mfcc(path_file, SAMPLE_RATE), NUM_FRAMES)
    predict_recorded = model.m.predict(np.expand_dims(mfcc_recorded, axis=0))
    return(predict_recorded)

def speaker_recognizer(path_file,path_model,path_predict_data): ## flac file
    predict_recorded = transform_voice(path_file,path_model)
    name_speaker = "UNKNOWN"
    best_score = 0 
    list_names = []
    for name in os.listdir(path_predict_data):
        for f in os.listdir(os.path.join(path_predict_data, name)):
            if f.endswith(".txt") == False : 
                score = batch_cosine_similarity(load(os.path.join(path_predict_data, name,f)),predict_recorded)
                if score > best_score: 
                    name_speaker = name
                    best_score = score
        list_names.append(name)
    return(name_speaker,round(best_score.item(),2),predict_recorded,list_names)
