import numpy as np
import random
from audio import read_mfcc
from batcher import sample_from_mfcc
from constants import SAMPLE_RATE, NUM_FRAMES
from conv_models import DeepSpeakerModel
from test import batch_cosine_similarity
import time
from numpy import save

np.random.seed(123)
random.seed(123)
path="/home/massal/Development/dtk-voice/3rd"
model = DeepSpeakerModel()
model.m.load_weights('ResCNN_triplet_training_checkpoint_265.h5', by_name=True)
chosen_file_name = '4-01'
mfcc_chosen = sample_from_mfcc(read_mfcc(path+'/Test_audio_files/'+chosen_file_name+".flac", SAMPLE_RATE), NUM_FRAMES)
predict_chosen = model.m.predict(np.expand_dims(mfcc_chosen, axis=0))

names_files=['4-00','4-02','4-01','3-00','3-01','3-02','3-03','2-01','2-02','2-03','2-04','1-00','1-01','1-02','1-03','1-04','1-05','1-06','Sudhanshu','Sudhashu2','Vik2','Vik','Vik3','librivox-p2zz','librivox-p0zz','men1','men','women','women1','women2','women3','women4','women5','m1','m2','m3','m4','m5','a1','a2','a3','a4','a5']

name_files_without_chosen= names_files.copy()
name_files_without_chosen.remove(chosen_file_name)
score_dict = {}
mfcc_dic={}
predict_dic={}
print("debut: "+str(time.perf_counter()))
from numpy import load

for file in name_files_without_chosen: 
    mfcc = sample_from_mfcc(read_mfcc(path+'/Test_audio_files/'+file+'.flac', SAMPLE_RATE), NUM_FRAMES)
    mfcc_dic[file]=mfcc
    predict_dic[file]=model.m.predict(np.expand_dims(mfcc_dic[file], axis=0))
    save(file+'predict.npy',model.m.predict(np.expand_dims(mfcc_dic[file], axis=0)))

print("sample: "+str(time.perf_counter()))

for file in name_files_without_chosen: 
    score = batch_cosine_similarity(load(file+'predict.npy'),predict_chosen)
    score_dict[file] = score.item(0)
print("score: "+str(time.perf_counter()))

print(sorted(score_dict.items(), key=lambda kv: kv[1]))

#

##mfcc_001 = sample_from_mfcc(read_mfcc(path+'/Test_audio_files/Vik.flac', SAMPLE_RATE), NUM_FRAMES)
##mfcc_002 = sample_from_mfcc(read_mfcc(path+'/Test_audio_files/Vik2.flac', SAMPLE_RATE), NUM_FRAMES)
##
##predict_001 = model.m.predict(np.expand_dims(mfcc_001, axis=0))
##predict_002 = model.m.predict(np.expand_dims(mfcc_002, axis=0))
##
##mfcc_003 = sample_from_mfcc(read_mfcc(path+'/Test_audio_files/librivox-p2zz.flac', SAMPLE_RATE), NUM_FRAMES)
##predict_003 = model.m.predict(np.expand_dims(mfcc_003, axis=0))
##
##print('SAME SPEAKER', batch_cosine_similarity(predict_001, predict_002))
##print('DIFF SPEAKER', batch_cosine_similarity(predict_001, predict_003))
##