from mycroft.skills.core import MycroftSkill, intent_handler
from mycroft.messagebus import Message
from mycroft.util.log import LOG
from adapt.intent import IntentBuilder
import sys
import numpy as np 
import os
from mycroft.util.deep_speaker import *
import time
from shutil import copyfile
class SEDValuesSkill(MycroftSkill):
    def __init__(self):
        super().__init__(name= "SEDValuesSkill")

    
    @intent_handler(IntentBuilder("SEDValues").require("Values").require("Team"))
    def handle_recognition(self,message):
        
        self.speak_dialog("values.are")
        time.sleep(4)
        self.speak_dialog("list.values")
                              

def create_skill():
    return SEDValuesSkill()
