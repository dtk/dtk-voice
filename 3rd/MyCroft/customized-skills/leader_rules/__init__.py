from mycroft.skills.core import MycroftSkill, intent_handler
from mycroft.messagebus import Message
from mycroft.util.log import LOG
from adapt.intent import IntentBuilder
import sys
import numpy as np 
import os
from mycroft.util.deep_speaker import *
import time
from shutil import copyfile
import random
class LeaderRulesSkill(MycroftSkill):
    def __init__(self):
        super().__init__(name= "LeaderRulesSkill")    
    @intent_handler(IntentBuilder("Leaderrule").require("rules").require("leader"))
    def handle_recognition(self,message):
        transcription = message.data['utterance']
        rule_number = random.randint(1,10)
        numbers = {"first":1,"second":2,"third":3,"fourth":4,"fifth":5, "sixth":6, "seventh":7,"eighth":8,"ninth":9,"tenth":10}
        for number in numbers : 
            if number in transcription : 
                rule_number = numbers[number]


        self.speak_dialog("rule_"+str(rule_number))
        
                              

def create_skill():
    return LeaderRulesSkill()
