from mycroft.skills.core import MycroftSkill, intent_handler
from mycroft.messagebus import Message
from mycroft.util.log import LOG
from adapt.intent import IntentBuilder
import sys
import numpy as np 
import os
from mycroft.util.deep_speaker import *
import time
from shutil import copyfile
class SpeakerRecognitionSkill(MycroftSkill):
    def __init__(self):
        super().__init__(name= "SpeakerRecognitionSkill")
        self.speaker_recognition_path = os.path.dirname(os.path.abspath(__file__))
        self.path_predict_data = os.path.dirname(os.path.dirname(self.speaker_recognition_path))+"/predict_data"

        self.threshold = THRESHOLD_PREDICTION
    def speak_wait(self, sentence,seconds,response = False): 
        resp = ""
        self.speak_dialog(sentence)
        time.sleep(seconds)
        if response : 
            resp = self.get_response("", data = None)
        return(resp)

    def repeat_sentence(self, sentence, time, counter,folder_name_path): 
        sentence_spoken = self.speak_wait(sentence,time,True)
        sentence_spoken = sentence_spoken.replace(" ", "").lower()
        predict_recorded = speaker_recognition.transform_voice("/tmp/voice_"+sentence_spoken+"_.flac",os.path.join(self.speaker_recognition_path,
                                                 'ResCNN_triplet_training_checkpoint_265.h5'))

        bf = open (os.path.join(folder_name_path , counter +'.npy'),'wb')
        np.save(bf, predict_recorded)

        
    @intent_handler(IntentBuilder("RecognizeVoice").require("WhoamI"))
    def handle_recognition(self,message):
        transcription = message.data['utterance'].replace(" ", "")
        name_speaker,score, predict_recorded,list_names = speaker_recognition.speaker_recognizer("/tmp/voice_"+transcription+"_.flac",os.path.join(self.speaker_recognition_path,
                                                 'ResCNN_triplet_training_checkpoint_265.h5'), self.path_predict_data)
        if score < THRESHOLD_PREDICTION :
            resp = self.speak_wait("can.not.recognize",6,True)
            if resp == "yes": 
                name = self.speak_wait("tell.your.name",3,True)    
                if name is None :
                    self.speak_dialog("cannot.register.voice")
                else: 
                    folder_name_path = self.path_predict_data +"/"+ name
                    if name in list_names : 
                        f_counter = open (os.path.join(folder_name_path, "counter.txt"),'rw')
                        counter = int(f_counter.read().strip())+1
                        transformed_counter = '%03d' % counter
                        f_counter.write(str(counter))
                        f_counter.close()
                        with open (os.path.join(folder_name_path, transformed_counter+'.npy'),'wb') as f: 
                            np.save(f, predict_recorded)
                        self.speak("voice.added.successfully")

                    else : 
                        os.mkdir(folder_name_path)
                        counter = "000" 
                        bf = open (os.path.join(folder_name_path , counter +'.npy'),'wb')
                        np.save(bf, predict_recorded)
                        f_counter = open (os.path.join(folder_name_path,"counter.txt"),'w') 
                        print ("created file ",os.path.join(folder_name_path,"counter.txt"))
                        collect = self.speak_wait("new.speaker.ask.recording", 10, True)
                        
                        if collect == "yes" :
                            self.speak_wait("start.recording",7, False)
                            self.repeat_sentence("sentence.1", 5, "001",folder_name_path)
                            self.repeat_sentence( "sentence.2",7, "002",folder_name_path)
                            self.repeat_sentence("sentence.3", 5, "003",folder_name_path)
                            f_counter.write("3")
                            self.speak_wait("thank.contribution", 5, False)
                        elif collect == "no": 
                            self.speak_dialog ("Okay")
                            f_counter.write("0")
                            print("write in file 0")
                        f_counter.close()


            elif resp == "no": 
                self.speak_wait("no.voice.saved",7)
        else : 
            self.speak_dialog('you.are', {'name': name_speaker, 'score': score })
                              

def create_skill():
    return SpeakerRecognitionSkill()
