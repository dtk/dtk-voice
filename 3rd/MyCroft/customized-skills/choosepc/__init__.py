from adapt.intent import IntentBuilder
from mycroft import MycroftSkill, intent_handler

class ChoosepcSkill(MycroftSkill):
    @intent_handler(IntentBuilder("Choosepc").require("Type"))
    def handle_do_you_like(self, message):
        potato_type = message.data.get('Type')
        if potato_type is not None:
            self.speak_dialog('pc.is.chosen',
                              {'type': potato_type})
        else:
            self.speak_dialog('ok')

def create_skill():
    return ChoosepcSkill()
