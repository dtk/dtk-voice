import os
import signal
from subprocess import Popen, PIPE, check_output
import time
from flask import Flask, request, Response
def read_current_output(process,bool):
    
    while True:
         
        output = process.stderr.readline()
        
        print(output.decode().strip())
        if "Waiting the input in the format" in output.decode():
            break
        #return(output.decode().strip())

def create_process(cmd):
    print("creating process")
    process = Popen([cmd],
                    stdin=PIPE, stdout=PIPE, stderr=PIPE,
                    shell=True) 
    read_current_output(process, False)
    return process


def run_inference(audio_path, process):
    start_time = time.time()
    process.stdin.write("{}\n".format(audio_path).encode())
    process.stdin.flush()
    while True:
        output = process.stderr.readline() 
        if "Inference tutorial for CTC" not in output.decode():
            return (output.strip())
        






# you can switch here to small model am_conformer_ctc_stride3_letters_25Mparams.bin
# set for it also lm_weight=2 and word_score=0
inference_cmd = """./build/bin/asr/fl_asr_tutorial_inference_ctc \
  --am_path=am_transformer_ctc_stride3_letters_300Mparams.bin \
  --tokens_path=tokens.txt \
  --lexicon_path=lexicon.txt \
  --lm_path=lm_common_crawl_small_4gram_prun0-6-15_200kvocab.bin \
  --logtostderr=true \
  --sample_rate=16000 \
  --beam_size=50 \
  --beam_size_token=30 \
  --beam_threshold=100 \
  --lm_weight=1.5 \
  --word_score=0"""
inference_flashlight = create_process(inference_cmd)
app = Flask(__name__)

@app.route("/stt",methods = ['POST'])
def handle(): 
    # 1. Read the body
    audio_data = request.data
    # 2. Put .wav in a random path = /tmp/xsadas.wav
    with open("/tmp/audio.wav", 'wb') as file:
        file.write(audio_data)
        
    # 3. flashlight.write("/tmp/xsadas.wav")
    output_inference = run_inference("/tmp/audio.wav", inference_flashlight)

    return(Response(output_inference))
    #return output_inference


if __name__ == '__main__':
    app.run(host='0.0.0.0')
